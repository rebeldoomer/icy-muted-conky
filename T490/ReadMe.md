conky 1.12.2 compiled 2022-02-23 for Linux x86_64

Compiled in features:

System config file: /etc/conky/conky.conf
Package library path: /usr/lib/conky


 General:
  * math
  * hddtemp
  * portmon
  * IPv6
  * IRC
  * Curl
  * RSS
  * ICal
  * iconv
  * Weather (METAR)
  * wireless
  * support for IBM/Lenovo notebooks
  * nvidia
  * builtin default configuration
  * old configuration syntax
  * Imlib2
  * OSS mixer support
  * apcupsd
  * iostats
  * ncurses
  * Internationalization support
  * PulseAudio

 Lua bindings:
  * Cairo
  * Imlib2
  * RSVG
 X11:
  * Xdamage extension
  * Xinerama extension (virtual display)
  * Xshape extension (click through)
  * XDBE (double buffer extension)
  * Xft
  * ARGB visual
  * Own window
  * HSV Gradient

 Music detection:
  * Audacious
  * CMUS
  * MPD
  * MOC
  * XMMS2

 Default values:
  * Netdevice: eno1
  * Local configfile: $HOME/.conkyrc
  * Localedir: /usr/share/locale
  * Maximum netdevices: 256
  * Maximum text size: 16384
  * Size text buffer: 256
